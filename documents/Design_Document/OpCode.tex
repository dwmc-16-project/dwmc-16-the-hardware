\chapter{Basic Operations and OpCodes}

This section contains a basic design of the \dwmc Operations and their OpCode design, meaning how operations are going to be represented in memory.

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{8}{Control Code} \\
	\end{bytefield}\\
	\begin{description}
		\item[OM] Operation Mode
	\end{description}
	\caption{General Operation Layout}
	\normalfont\normalsize
\end{figure}

All operations share the same general OpCode Layout, consisting of a 6 bit OpCode, which allows for up to 64 different operations, followed by a 2 bit Operation Mode, which is needed to enable, for example, different memory addressing schemes, and finally an 8 bit Control Code, which in turn depends on the Operation Mode.

Generally, there are four different typed of operations:

\begin{itemize}
	\item Data Transfer Operations
	\item Arithmetic Logical Operations
	\item Control Operations
	\item Other Operations
\end{itemize}

\section{Data Transfer Operations}

Data Transfer Operations are perhaps the most important operations within the \dwmc Instruction Set, as they are responsible for moving data between the memory and the CPU, as well as between registers. However, there are only three data transfer operations.

\vspace{1em}
\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Operation} & \textbf{Mnemonic} &                                        \\ \hline\hline
		       Load        &        LD         &   Load data from Memory to Register    \\ \hline
		      Store        &        ST         &   Store data from Register to Memory   \\ \hline
		       Move        &        MOV        &  Move data from register to register   \\ \hline
		       Push        &       PUSH        & Push Value from Register Rs to Stack S \\ \hline
		       Pop         &        POP        & Pop value from Stack S to Register Rd  \\ \hline
		 Increment Offset  &       INCO        &  Increment value of Offset Register N  \\ \hline
		 Decrement Offset  &       DECO        &  Decrement value of Offset Register N  \\ \hline
	\end{tabular} 
	\caption{Data Transfer Operations}
	\normalfont\normalsize	
\end{table}

Internally, these operations are separated into two groups with \texttt{LD} and \texttt{ST} falling into the memory operations, while \texttt{MOV} is a register operation.

\subsection{Memory Operations} \label{memory_ops}

The Memory Operations help define the different types of data access modes that will be used within the \dwmc Instruction Set by other operations, so the memory address modes will be defines in this subsection.

The general OpCode design has already been defined earlier and will be used here.

\vspace{1em}
\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Operation Mode} &             Memory Address Mode              \\ \hline\hline
		          00            &                Register Mode                 \\ \hline
		          01            &                 Direct Mode                  \\ \hline
		          10            &            Absolute Address Mode             \\ \hline
		          11            & Other Modes (see Table \ref{tab:other_mode}) \\ \hline
	\end{tabular}
	\caption{Memory Address Modes}
	\label{tab:memory_address_mode}
\end{table}

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{8}{Control Code} \\
		\begin{rightwordgroup}{Register Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Rs}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Direct Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{1}  & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
			\bitbox{16}{Memory Value}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Absolute Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{0}  & \bitbox{4}{Rd} & \bitbox{4}{Addr[19:16]}\\
			\bitbox{16}{Addr[15:0]}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Other Modes}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{1}  & \bitbox{4}{Rd} & \bitbox{4}{Addr Mode}\\
			\bitbox{16}{Memory Value (optional)}
		\end{rightwordgroup} \\
	\end{bytefield}\\
	\begin{description}
		\item[OM] Operation Mode
		\item[Rd] Destination Register
		\item[Rs] Source Register
		\item[Addr] 20 bit Address
		\item[Addr Mode] Other Modes (see Table \ref{tab:other_mode})
	\end{description}
	\caption{OpCode Layouts for Memory Modes}
	\normalfont\normalsize
\end{figure}

\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Address Mode} &            Memory Address Mode             \\ \hline\hline
		        0000          &             Local Address Mode             \\ \hline
		        1000          &            Index W Address Mode            \\ \hline
		        1001          &            Index X Address Mode            \\ \hline
		        1010          &            Index Y Address Mode            \\ \hline
		        1011          &            Index Z Address Mode            \\ \hline
		        1100          &      Offset w Address Mode (register)      \\ \hline
		        1101          &      Offset X Address Mode (register)      \\ \hline
		        1110          &      Offset Y Address Mode (register)      \\ \hline
		        1111          &      Offset Z Address Mode (register)      \\ \hline
	\end{tabular}
	\caption{Other Memory Address Modes}
	\label{tab:other_mode}
\end{table}

There are several different Address Modes encoded in the 2 bit Operation Mode and the four bit of the Control Code used by the 'Other Modes':

\begin{description}
	\item[Register Mode] Register to Register Mode. This mode is not used by \texttt{LD} and \texttt{ST}, but almost exclusively by Arithmetic Logical Operations and Branch Operations.
	\item[Direct Mode] The Direct Mode loads the 16 bit value from the data word following the operation into the destination register.
	\item{Absolute Address Mode} This Mode uses the lower four bit \texttt{[3:0]} of the Control to encode the upper four bit \texttt{[19:16]} of the 20 bit address, while the data word following the operation encodes the lower 16 bit \texttt{[15:0]} of the 20 bit address. This allows to address the full memory space of the system.
	\item[Local Address Mode] The local address Mode allows to address only the local 64 kiWord address space, with the upper 4 bit \texttt{[19:16]} taken from the systems Program Counter.
	\item[Index W/X/Y/Z Address Mode] In this addressing mode, the system uses the content of the W/X/Y/Z Index Register to point towards a memory cell to read data.
	\item[Offset W/X/Y/Z Address Mode] Using this mode, the system is able to read from a list of data, relative to the address to the W/X/Y/Z Index Register, allowing access to lists/arrays of data. This can either be done by the register, or using a more direct method, where the command itself loads the data word following the operation directly into the W/X/Y/Z Offset Register.
\end{description}

\subsection{Stack Operations}

The FOUR Stack Operations allow the System to operate over the System Stack, as well as the four secondary Stacks, which are created by use of the Index Registers in combination with their Offset Registers.

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd/Rs} & \bitbox{3}{Stack} & \bitbox{1}{0}
	\end{bytefield}\\
	\begin{description}
		\item[Rd] Destination Register
		\item[Rs] Source Register
	\end{description}
	\caption{Stack Operation Layouts}
	\normalfont\normalsize
\end{figure}

\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Stack} &                                       \\ \hline\hline
		000       & \multirow{4}{*}{System Stack Pointer} \\ \cline{1-1}
		001       &                                       \\ \cline{1-1}
		010       &                                       \\ \cline{1-1}
		011       &                                       \\ \hline
		100       &       Secondary Stack Pointer W       \\ \hline
		101       &       Secondary Stack Pointer X       \\ \hline
		110       &       Secondary Stack Pointer Y       \\ \hline
		111       &       Secondary Stack Pointer Z       \\ \hline
	\end{tabular}
	\caption{Stack Pointer Addresses for \texttt{PUSH/POP}}
	\label{tab:stack_pointer_address}
\end{table}

\subsection{Register Operations}

There is only a single Register Operation, which is capable of moving data from a Source Register to a Destination Register, with the possibility of moving data between the two register pages.

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{4}{Rd} & \bitbox{4}{Rs} \\
	\end{bytefield}\\	
	\begin{description}
		\item[OM] Operation Mode
		\item[Rd] Destination Register
		\item[Rs] Source Register
	\end{description}
	\caption{Register Operation Layout}
	\normalfont\normalsize
\end{figure}

\vspace{1em}
\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Operation Mode} &                           \\ \hline\hline
		          00            &   Current Register Page   \\ \hline
		          01            & Register Page 1 to Page 2 \\ \hline
		          10            & Register Page 2 to Page 1 \\ \hline
		          11            &           N.A.            \\ \hline
	\end{tabular}
	\caption{Register Modes}
\end{table}
\mbox{}

\clearpage
\section{Control Operations}

Control Operations are responsible for allowing the \dwmc CPU to make decisions based on data, and move around inside the code. Some of these operations can use the Memory Modes defined in Section \ref{memory_ops}.

\vspace{1em}
\begin{table}[h!]
	\centering
	\ttfamily\scriptsize
	\begin{tabular}{|c|c|c|}
		\hline
		      \textbf{Operation}       & \textbf{Mnemonic} &                                             \\ \hline\hline
		      Branch if Bit Set        &        BB         &        Branch if Bit N in Rs1 is set        \\ \hline
		       Branch if Equal         &        BE         &       Branch if Rs1 and Rs2 are equal       \\ \hline
		     Branch if Less then       &        BL         &       Branch if Rs1 is less then Rs2        \\ \hline
		   Branch if Less or Equal     &        BLE        &    Branch if RS1 id less or equal to Rs2    \\ \hline
		        Branch if Zero         &        BZ         &             Branch if Rs1 is 0              \\ \hline
		Branch if Zero, post decrement &        BZD        & Branch if Rs1 is 0, otherwise decrement Rs1 \\ \hline
		             Jump              &        JMP        &               Jump to address               \\ \hline
		      Jump to Subroutine       &        JMS        &        Jump to subroutine at address        \\ \hline
		    Return from Subroutine     &        RET        &          Returns from a Subroutine          \\ \hline
		    Return from Interrupt      &        RTI        &          Returns from an Interrupt          \\ \hline
		         No Operation          &        NOP        &                No Operation                 \\ \hline
		             Wait              &       WAIT        &             Wait for Interrupt              \\ \hline
		             DMA               &        DMA        &      Wait for DMA operation to finish       \\ \hline
	\end{tabular}
	\begin{description}
		\item[Rd] Destination Register
		\item[Rs] Source Register
	\end{description}
	\caption{Control Operations}
	\normalfont\normalsize	
\end{table}

Again, there are two types of operations, conditional branch operations and unconditional jump operations.

\subsection{Branch Operations}

The six available branch operations are subdivided into two types, branch operations with a single operand \texttt{BZ} and \texttt{BZI}, as well as those with two operands \texttt{BB}, \texttt{BE}, \texttt{BL} and \texttt{BLE}.

To simplify the OpCode design, it is assumed that Branch operations are jumping to a relative address. This means that the branch target will remain within a 16 bit address window, allowing for a jump range of $\pm32$ kiWord, using a signed 16 value.

\subsubsection{Single Operand}

Single Operand Branches are using just a single Source Register, which simplified their OpCode. Like in the Single Operand Arithmetic Logical Operations, the Operation Mode it set to \texttt{00}, while the lowest four bit are set to \texttt{0000}.

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
		\bitbox{16}{Relative Jump Offset}
	\end{bytefield}\\
	\begin{description}
		\item[Rd] Destination Register
	\end{description}
	\caption{Single Operand Branch Operation Layout}
	\normalfont\normalsize
\end{figure}

\subsubsection{Two Operands}

Much like the Two Operand Arithmetic Logical Operations the Two Operand Branch Operations make use of the Memory Modes defined in Section \ref{memory_ops}, though they are extended by the one word of the value for the relative jump.

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{8}{Control Code} \\
		\begin{rightwordgroup}{Register Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Rs}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Direct Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{1} & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
			\bitbox{16}{Memory Value}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Absolute Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Addr[19:16]}\\
			\bitbox{16}{Addr[15:0]}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Other Modes}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{1} & \bitbox{4}{Rd} & \bitbox{4}{Addr Mode}\\
			\bitbox{16}{Memory Value (optional)}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
	\end{bytefield}\\
	\begin{description}
		\item[OM] Operation Mode
		\item[Rd] Destination Register
		\item[Rs] Source Register
		\item[Addr] 20 bit Address
		\item[Addr Mode] Other Modes (see Table \ref{tab:other_mode})
	\end{description}
	\caption{Two Operand Arithmetic Logical Operation Layouts}
	\normalfont\normalsize
\end{figure}

\subsection{Unconditional Jumps}

There are four available unconditional jump operations, which are once against subdivided into two types, jumps and returns, of which the jumps have a single operand, while the return operations have no operand, using the stack to get their return absolute address.

\subsubsection{Jump Operations}

Jump operations themselves make use of a modified version of the Memory Modes as defined in Section \ref{memory_ops}. However, they do not make use of the Register or Direct Address Modes, and do not use a Destination Register.

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\begin{rightwordgroup}{Absolute Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Addr[19:16]}\\
			\bitbox{16}{Addr[15:0]}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Other Modes}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{1} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Addr Mode}\\
			\bitbox{16}{Memory Value (optional)}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
	\end{bytefield}\\
	\begin{description}
		\item[Addr] 20 bit Address
		\item[Addr Mode] Other Modes (see Table \ref{tab:other_mode})
	\end{description}
	\caption{Jump Operation Layouts}
	\normalfont\normalsize
\end{figure}

\subsubsection{Return Operations}

The Return Operations do not use any operand and are therefore just a simple six bit opcode, followed by ten \texttt{0}.

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
	\end{bytefield}\\
	\caption{Return Operation Layout}
	\normalfont\normalsize
\end{figure}

\subsection{Other Control Operations}

The remaining control operations are making use of memory modes.

The OpCodes Layout of these operations are simplified, as they are operations meant to wait, either for the next operation, or for an external event, be it a return of memory access from the DMA Controller, or an external interrupt. 

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\begin{rightwordgroup}{NOP/WAIT/DMA}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0}
		\end{rightwordgroup} \\
	\end{bytefield}
	\caption{Other Operation Layouts}
	\normalfont\normalsize
\end{figure}

\newpage
\section{Arithmetic Logical Operations}

Arithmetic Logical Operations are operations that are done by the ALU of the \dwmc CPU. They are used to modify the data, and are able to read from memory directly, based around the Memory Modes defined in Section \ref{memory_ops}.

\vspace{1em}
\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Operation} & \textbf{Mnemonic} &                                     \\ \hline\hline
		Add (with Carry)        &        ADD/ADDC         &  Add Rs to Rd (with Carry)  \\ \hline
		Increment        &        INC         & Increment Rd \\ \hline
		Subtract (with Carry)        &        SUB/SUBC        & Substract Rs from Rd  \\ \hline
		Decrement & DEC & Decrement Rd \\ \hline
		Logical AND/OR/XOR & AND/OR/XOR & Logical AND/OR/XOR of Rd and Rs \\ \hline
		Logical NOT & NOT & Logical NOT of Rd \\ \hline
		Logical Left/Right Shift & LLS/LRS & Logical Left/Right Shift of Rd with Carry \\ \hline
		Logical Left/Right Rotate & LLR/LRR & Locigal Left/Right Shift of Rd \\ \hline
		Set/Reset Bit & SB/RB & Set/Reset Bit N in Rd \\ \hline
	\end{tabular}
	\begin{description}
		\item[Rd] Destination Register
		\item[Rs] Source Register
	\end{description}
	\caption{Arithmetic Logical Operations}
	\normalfont\normalsize	
\end{table}

There are two types of arithmetic logical operations, those with a single operand and those with two operands. \texttt{INC}, \texttt{DEC}, \texttt{NOT}, \texttt{LLS/LRS} and \texttt{LLR/LRR} are operations with one operand, while \texttt{AND/ANDC}, \texttt{SUB/SUBC}, \texttt{AND/OR/XOR} and \texttt{SB/RB} are operations with two operands. In this \texttt{SB/RB} are slightly different and do not use the same opcode as the other two operand operations.

\subsection{Single Operand}

Single Operand Operations have a simple OpCode, where the Operation Mode is fixed to \texttt{00} and only the Destination Register is used.

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
	\end{bytefield}\\
	\begin{description}
		\item[Rd] Destination Register
	\end{description}
	\caption{Single Operand Arithmetic Logical Operation Layout}
	\normalfont\normalsize
\end{figure}

\subsection{Two Operands}

The two Operand Operations are largely using the Memory Modes as defines in Section \ref{memory_ops}. The only expectation are the \texttt{SB/RB} operations.

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{8}{Control Code} \\
		\begin{rightwordgroup}{Register Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Rs}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Direct Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{1} & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
			\bitbox{16}{Memory Value}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Absolute Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Addr[19:16]}\\
			\bitbox{16}{Addr[15:0]}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Other Modes}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{1} & \bitbox{4}{Rd} & \bitbox{4}{Addr Mode}\\
			\bitbox{16}{Memory Value (optional)}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{SB/RB}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Bit/Flag}
		\end{rightwordgroup}
	\end{bytefield}\\
	\begin{description}
		\item[OM] Operation Mode
		\item[Rd] Destination Register
		\item[Rs] Source Register
		\item[Addr] 20 bit Address
		\item[Addr Mode] Other Modes (see Table \ref{tab:other_mode})
	\end{description}
	\caption{Two Operand Arithmetic Logical Operation Layouts}
	\normalfont\normalsize
\end{figure}

\newpage
\section{Memory Mode Mnemonics}

Finally, there needs to be some thought put into the design of the Mnemonics used to designate the Memory Modes as defined in Section \ref{memory_ops} for use in an Assembler program.

\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|l|}
		\hline
		\textbf{Addressing Mode} & \textbf{Mnemonic} \\ \hline\hline
		Register Mode & ADD R00, R01 \\ \hline
		Direct Mode & ADD R00, 0x00FF \\ \hline
		Local Addressing Mode (16 bit Address)& ADD R00, @0x00FF \\ \hline
		Absolute Addressing Mode (20 bit address)& ADD R00, @0x100FF \\ \hline
		Index W/X/Y/Z Address Mode & ADD R00, @W \\ \hline
		Offset W/X/Y/Z Address Mode (offset register) & ADD R00, +@W \\ \hline
	\end{tabular}
	\caption{Addressing Mode Mnemonics}
	\normalfont\normalsize
\end{table}