\chapter{Instruction Set}

With the work on the Operation Codes done, a closer look at the Instruction Set is needed, collating all the information in a more compact way.

\section{General information}

This section is largely used to collect the information about the general layout of the Operation Codes and the Address Modes again.

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{8}{Control Code} \\
	\end{bytefield}\\
	\begin{description}
		\item[OM] Operation Mode
	\end{description}
	\caption{General Operation Layout}
	\normalfont\normalsize
\end{figure}

\subsection{Address Modes} \label{memory_ops_8}

The Address Modes are used to define the way an operation addresses the Memory of the \dwmc. These Address Modes are used by almost all operations of the Instruction Set in one way or another

\vspace{1em}
\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Operation Mode} &          Memory Address Mode           \\ \hline\hline
		00            &             Register Mode              \\ \hline
		01            &          Direct Mode                   \\ \hline
		10            &         Absolute Address Mode          \\ \hline
		11            & Other Modes (see Table \ref{tab:other_mode_8}) \\ \hline
	\end{tabular}
	\caption{Memory Address Modes}
	\label{tab:memory_address_mode_8}
\end{table}

\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Address Mode} &            Memory Address Mode             \\ \hline\hline
		0000          &             Local Address Mode             \\ \hline
		1000          &            Index W Address Mode            \\ \hline
		1001          &            Index X Address Mode            \\ \hline
		1010          &            Index Y Address Mode            \\ \hline
		1011          &            Index Z Address Mode            \\ \hline
		1100          &      Offset w Address Mode (register)      \\ \hline
		1101          &      Offset X Address Mode (register)      \\ \hline
		1110          &      Offset Y Address Mode (register)      \\ \hline
		1111          &      Offset Z Address Mode (register)      \\ \hline
	\end{tabular}
	\caption{Other Memory Address Modes}
	\label{tab:other_mode_8}
\end{table}

\begin{description}
	\item[Register Mode] Register to Register Mode. This mode is not used by \texttt{LD} and \texttt{ST}, but almost exclusively by Arithmetic Logical Operations and Branch Operations.
	\item[Direct Mode] The Direct Mode loads the 16 bit value from the data word following the operation into the destination register.
	\item{Absolute Address Mode} This Mode uses the lower four bit \texttt{[3:0]} of the Control to encode the upper four bit \texttt{[19:16]} of the 20 bit address, while the data word following the operation encodes the lower 16 bit \texttt{[15:0]} of the 20 bit address. This allows to address the full memory space of the system.
	\item[Local Address Mode] The local address Mode allows to address only the local 64 kiWord address space, with the upper 4 bit \texttt{[19:16]} taken from the systems Program Counter.
	\item[Index W/X/Y/Z Address Mode] In this addressing mode, the system uses the content of the W/X/Y/Z Index Register to point towards a memory cell to read data.
	\item[Offset W/X/Y/Z Address Mode] Using this mode, the system is able to read from a list of data, relative to the address to the W/X/Y/Z Index Register, allowing access to lists/arrays of data. This can either be done by the register, or using a more direct method, where the command itself loads the data word following the operation directly into the W/X/Y/Z Offset Register.
\end{description}

\subsection{System Stack}

The CPU of the \dwmc has access to four stacks, four of them basic System Stacks, as well as four secondary Stacks, which are created by combining the four Index Registers with their Offset registers.

\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Stack} &                                       \\ \hline\hline
		000       & \multirow{4}{*}{System Stack Pointer} \\ \cline{1-1}
		001       &                                       \\ \cline{1-1}
		010       &                                       \\ \cline{1-1}
		011       &                                       \\ \hline
		100       &       Secondary Stack Pointer W       \\ \hline
		101       &       Secondary Stack Pointer X       \\ \hline
		110       &       Secondary Stack Pointer Y       \\ \hline
		111       &       Secondary Stack Pointer Z       \\ \hline
	\end{tabular}
	\caption{Stack Pointer Addresses for \texttt{PUSH/POP}}
	\label{tab:stack_pointer_address_8}
\end{table}

\subsection{Register Modes}

Register Modes are used when an operation makes use of more then one Register, to be able to address the two active Register Pages of the CPU.

\vspace{1em}
\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Operation Mode} &                           \\ \hline\hline
		00            &   Current Register Page   \\ \hline
		01            & Register Page 1 to Page 2 \\ \hline
		10            & Register Page 2 to Page 1 \\ \hline
		11            &           N.A.            \\ \hline
	\end{tabular}
	\caption{Register Modes}
	\label{tab:reg_modes_8}
\end{table}

\subsection{Memory Mode Mnemonics}

\begin{table}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|l|}
		\hline
		\textbf{Addressing Mode} & \textbf{Mnemonic} \\ \hline\hline
		Register Mode & ADD R00, R01 \\ \hline
		Direct Mode & ADD R00, 0x00FF \\ \hline
		Local Addressing Mode (16 bit Address)& ADD R00, @0x00FF \\ \hline
		Absolute Addressing Mode (20 bit address)& ADD R00, @0x100FF \\ \hline
		Index W/X/Y/Z Address Mode & ADD R00, @W \\ \hline
		Offset W/X/Y/Z Address Mode (offset register) & ADD R00, +@W \\ \hline
	\end{tabular}
	\caption{Addressing Mode Mnemonics}
	\normalfont\normalsize
\end{table}

\newpage
\section{Data Transfer Operations}

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{8}{Control Code} \\
		\begin{rightwordgroup}{Register Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Rs}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Direct Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{1}  & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
			\bitbox{16}{Memory Value}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Absolute Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{0}  & \bitbox{4}{Rd} & \bitbox{4}{Addr[19:16]}\\
			\bitbox{16}{Addr[15:0]}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Other Modes}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{1}  & \bitbox{4}{Rd} & \bitbox{4}{Addr Mode}\\
			\bitbox{16}{Memory Value (optional)}
		\end{rightwordgroup} \\
	\end{bytefield}\\
	\begin{description}
		\item[OM] Operation Mode
		\item[Rd] Destination Register
		\item[Rs] Source Register
		\item[Addr] 20 bit Address
		\item[Addr Mode] Other Modes (see Table \ref{tab:other_mode_8})
	\end{description}
	\caption{OpCode Layouts for Memory Modes in \texttt{LD} and \texttt{ST}}
	\label{tab:DTO_LS}
	\normalfont\normalsize
\end{figure}

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{4}{Rd} & \bitbox{4}{Rs} \\
	\end{bytefield}\\	
	\begin{description}
		\item[OM] Operation Mode
		\item[Rd] Destination Register
		\item[Rs] Source Register
	\end{description}
	\caption{OpCode Layout for \texttt{MOV}}
	\label{ref:DTO_MOV}
	\normalfont\normalsize
\end{figure}

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd/Rs} & \bitbox{3}{Stack} & \bitbox{1}{0}
	\end{bytefield}\\
	\begin{description}
		\item[Rd] Destination Register
		\item[Rs] Source Register
	\end{description}
	\caption{OpCode Layout for \texttt{PUSH}, \texttt{POP}, \texttt{INCO}, \texttt{DECO}}
	\label{ref:DTO_ST}
	\normalfont\normalsize
\end{figure}

\newpage
\subsection{Load \texttt{LD}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{LD Rd, Addr/C}
	\item[Operation] \texttt{Rd $\Leftarrow$ Memory[Addr/C]}
	\item[OpCode] \texttt{0b000001xxxxxxxxxx} (see Table \ref{tab:DTO_LS})
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] None/All (if target is the Flag Register R15)
	\item[Cycles] 4 Clock Cycles
	\item Load data from Memory location Addr or a Constant into Register Rd
\end{basedescript}

\subsection{Store \texttt{ST}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{ST Rs, Addr}
	\item[Operation] \texttt{Memory[Addr] $\Leftarrow$ Rs}
	\item[OpCode] \texttt{0b000011xxxxxxxxxx} (see Table \ref{tab:DTO_LS})
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] None
	\item[Cycles] 4 Clock Cycles
	\item Store data from Register Rs into Memory location Addr
\end{basedescript}

\subsection{Move \texttt{MOV}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{MOV Rd, Rs}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rs}
	\item[OpCode] \texttt{0b000101xxxxxxxxxx} (see Table \ref{ref:DTO_MOV}) 
	\item[Address Modes] see Table \ref{tab:reg_modes_8}
	\item[Affected Flags] None/All (if target is the Flag Register R15)
	\item[Cycles] 4 Clock Cycles
	\item Move data from Register Rd into Register Rs
\end{basedescript}

\subsection{Push \texttt{PUSH}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{PUSH Rs, S}
	\item[Operation] \texttt{Stack[S] $\Leftarrow$ Rs}
	\item[OpCode] \texttt{0b001000xxxxxxxxxx} (see Table \ref{ref:DTO_ST}) 
	\item[Address Modes] None
	\item[Affected Flags] None
	\item[Cycles] 4 Clock Cycles
	\item Push data from Register Rs into Stack S
\end{basedescript}

\subsection{Pop \texttt{POP}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{POP Rd, S}
	\item[Operation] \texttt{Rd $\Leftarrow$ Stack[S]}
	\item[OpCode] \texttt{0b001001xxxxxxxxxx} (see Table \ref{ref:DTO_ST}) 
	\item[Address Modes] None
	\item[Affected Flags] None/All (if target is the Flag Register R15)
	\item[Cycles] 4 Clock Cycles
	\item Push data from Stack S into Register Rd
\end{basedescript}

\subsection{Increment Offset Register \texttt{INCO}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{INCO Osr}
	\item[Operation] \texttt{Osr $\Leftarrow$ Osr + 1}
	\item[OpCode] \texttt{0b001010xxxxxxxxxx} (see Table \ref{tab:other_ops}) 
	\item[Address Modes] None
	\item[Affected Flags] OF, C
	\item[Cycles] 4 Clock Cycles
	\item Increments Index Offset Register Osr
\end{basedescript}

\subsection{Decrement Offset Register \texttt{DECO}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{DECO Osr}
	\item[Operation] \texttt{Osr $\Leftarrow$ Osr - 1}
	\item[OpCode] \texttt{0b001011xxxxxxxxxx} (see Table \ref{tab:other_ops}) 
	\item[Address Modes] None
	\item[Affected Flags] OF, C, Z, N
	\item[Cycles] 4 Clock Cycles
	\item Decrements Index Offset Register Osr
\end{basedescript}

\newpage
\section*{Control Operations}

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
		\bitbox{16}{Relative Jump Offset}
	\end{bytefield}\\
	\begin{description}
		\item[Rd] Destination Register
	\end{description}
	\caption{Single Operand Branch Operation Layout}
	\label{tab:branch_single}
	\normalfont\normalsize
\end{figure}



\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{8}{Control Code} \\
		\begin{rightwordgroup}{Register Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Rs}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Direct Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{1} & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
			\bitbox{16}{Memory Value}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Absolute Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Addr[19:16]}\\
			\bitbox{16}{Addr[15:0]}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Other Modes}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{1} & \bitbox{4}{Rd} & \bitbox{4}{Addr Mode}\\
			\bitbox{16}{Memory Value (optional)}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
	\end{bytefield}\\
	\begin{description}
		\item[OM] Operation Mode
		\item[Rd] Destination Register
		\item[Rs] Source Register
		\item[Addr] 20 bit Address
		\item[Addr Mode] Other Modes (see Table \ref{tab:other_mode})
	\end{description}
	\caption{Two Operand Arithmetic Logical Operation Layouts}
	\label{tab:branch_double}
	\normalfont\normalsize
\end{figure}

\newpage
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\begin{rightwordgroup}{Absolute Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Addr[19:16]}\\
			\bitbox{16}{Addr[15:0]}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Other Modes}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{1} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Addr Mode}\\
			\bitbox{16}{Memory Value (optional)}\\
			\bitbox{16}{Relative Jump Offset}
		\end{rightwordgroup} \\
	\end{bytefield}\\
	\begin{description}
		\item[Addr] 20 bit Address
		\item[Addr Mode] Other Modes (see Table \ref{tab:other_mode})
	\end{description}
	\caption{Jump Operation Layouts}
	\label{tab:jump}
	\normalfont\normalsize
\end{figure}

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
	\end{bytefield}\\
	\caption{Return Operation Layout}
	\label{tab:return}
	\normalfont\normalsize
\end{figure}

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\begin{rightwordgroup}{NOP/WAIT/DMA}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0}
		\end{rightwordgroup} \\
	\end{bytefield}
	\caption{Waiting Operation Layout}
	\label{tab:wait}
	\normalfont\normalsize
\end{figure}

\newpage
\subsection{Branch if Bit Set \texttt{BB}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{BB Rd, F, Addr}
	\item[Operation] Relative Jump:\\\texttt{if Rd[F] == 1: PC $\Leftarrow$ PC + Addr}\\\texttt{else: PC $\Leftarrow$ PC + 2}\\
	Absolure Jump:\\\texttt{if Rd[F] == 1: PC $\Leftarrow$ Addr}\\\texttt{else: PC $\Leftarrow$ PC + 3}\\
	\item[OpCode] \texttt{0b011100xxxxxxxxxx} (see Table \ref{tab:branch_double}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] None
	\item[Cycles] 6 Clock Cycles
	\item Branch to Address Addr if Bit F in Register Rd is set, continue code if not
\end{basedescript}

\subsection{Branch if Equal \texttt{BE}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{BE Rd, Rs, Addr}
	\item[Operation] Relative Jump:\\\texttt{if Rd == Rs: PC $\Leftarrow$ PC + Addr}\\\texttt{else: PC $\Leftarrow$ PC + 2}\\
	Absolure Jump:\\\texttt{if Rd == Rs: PC $\Leftarrow$  Addr}\\\texttt{else: PC $\Leftarrow$ PC + 3}\\
	\item[OpCode] \texttt{0b011101xxxxxxxxxx} (see Table \ref{tab:branch_double}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] None
	\item[Cycles] 6 Clock Cycles
	\item Branch to Address Addr if Register Rd is equal to Register Rs, continue if not
\end{basedescript}

\subsection{Branch if Less \texttt{BL}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{BL Rd, Rs, Addr}
	\item[Operation] Relative Jump:\\\texttt{if Rd < Rs: PC $\Leftarrow$ PC + Addr}\\\texttt{else: PC $\Leftarrow$ PC + 2}\\
	Absolure Jump:\\\texttt{if Rd < Rs: PC $\Leftarrow$  Addr}\\\texttt{else: PC $\Leftarrow$ PC + 3}\\
	\item[OpCode] \texttt{0b011110xxxxxxxxxx} (see Table \ref{tab:branch_double}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] None
	\item[Cycles] 6 Clock Cycles
	\item Branch to Address Addr if Register Rd smaller then Register Rs, continue if not
\end{basedescript}

\subsection{Branch if Less or Equal \texttt{BLE}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{BLE Rd, Rs, Addr}
	\item[Operation] Relative Jump:\\\texttt{if Rd <= Rs: PC $\Leftarrow$ PC + Addr}\\\texttt{else: PC $\Leftarrow$ PC + 2}\\
	Absolure Jump:\\\texttt{if Rd <= Rs: PC $\Leftarrow$  Addr}\\\texttt{else: PC $\Leftarrow$ PC + 3}\\
	\item[OpCode] \texttt{0b011111xxxxxxxxxx} (see Table \ref{tab:branch_double}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] None
	\item[Cycles] 6 Clock Cycles
	\item Branch to Address Addr if Register Rd is smaller or equal to Register Rs, continue if not
\end{basedescript}

\subsection{Branch if Zero \texttt{BZ}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{BZ Rd, Addr}
	\item[Operation] Relative Jump:\\\texttt{if Rd == 0: PC $\Leftarrow$ PC + Addr}\\\texttt{else: PC $\Leftarrow$ PC + 2}\\
	Absolure Jump:\\\texttt{if Rd == 0: PC $\Leftarrow$  Addr}\\\texttt{else: PC $\Leftarrow$ PC + 3}\\
	\item[OpCode] \texttt{0b011000xxxxxxxxxx} (see Table \ref{tab:branch_single}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] Z
	\item[Cycles] 6 Clock Cycles
	\item Branch to Address Addr if Register Rd is zero, continue if not, Sets the Zero bit
\end{basedescript}

\subsection{Branch if Zero, Post Decrement \texttt{BZD}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{BZD Rd, Addr}
	\item[Operation] Relative Jump:\\\texttt{if Rd == 0: PC $\Leftarrow$ PC + Addr}\\\texttt{else: PC $\Leftarrow$ PC + 2; Rd $\Leftarrow$ Rd - 1}\\
	Absolure Jump:\\\texttt{if Rd == 0: PC $\Leftarrow$  Addr}\\\texttt{else: PC $\Leftarrow$ PC + 3; Rd $\Leftarrow$ Rd - 1}\\
	\item[OpCode] \texttt{0b011001xxxxxxxxxx} (see Table \ref{tab:branch_single}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] Z
	\item[Cycles] 6 Clock Cycles
	\item Branch to Address Addr if Register Rd is zero, continue if not and decrement by 1, Sets the Zero bit
\end{basedescript}

\subsection{Jump \texttt{JMP}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{JMP Addr}
	\item[Operation] Relative Jump:\\\texttt{PC $\Leftarrow$ PC + Addr}\\
	Absolure Jump:\\\texttt{PC $\Leftarrow$ Addr}\\
	\item[OpCode] \texttt{0b010100xxxxxxxxxx} (see Table \ref{tab:jump}) 
	\item[Address Modes] None
	\item[Affected Flags] None
	\item[Cycles] 4 Clock Cycles
	\item Jump to Address Addr
\end{basedescript}

\subsection{Jump to Subroutine \texttt{JMS}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{JMS Addr}
	\item[Operation] Relative Jump:\\\texttt{Stack[0] $\Leftarrow$ PC; PC $\Leftarrow$ PC + Addr}\\
	Absolure Jump:\\\texttt{Stack[0] $\Leftarrow$ PC; PC $\Leftarrow$ Addr}\\
	\item[OpCode] \texttt{0b010101xxxxxxxxxx} (see Table \ref{tab:jump}) 
	\item[Address Modes] None
	\item[Affected Flags] None
	\item[Cycles] 4 Clock Cycles
	\item Jump to Subroutine at Address Addr, saves Program Counter to the System Stack 0
\end{basedescript}

\subsection{Return from Subroutine \texttt{RET}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{RET}
	\item[Operation] \texttt{PC $\Leftarrow$ Stack[0]}
	\item[OpCode] \texttt{0b010000xxxxxxxxxx} (see Table \ref{tab:return}) 
	\item[Address Modes] None
	\item[Affected Flags] None
	\item[Cycles] 4 Clock Cycles
	\item Returns from a subroutine, jumping to the address saved in the System Stack 0
\end{basedescript}

\subsection{Return from Interrupt \texttt{RTI}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{RTI}
	\item[Operation] \texttt{Registers $\Leftarrow$ Stack[0]; PC $\Leftarrow$ Stack[0]}
	\item[OpCode] \texttt{0b010001xxxxxxxxxx} (see Table \ref{tab:return}) 
	\item[Address Modes] None
	\item[Affected Flags] None
	\item[Cycles] N Clock Cycles
	\item Returns from an interrupt handling routine, rewrites Registers from Stack, jumping to the address saved in the System Stack 0
\end{basedescript}

\subsection{No Operation \texttt{NOP}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{NOP}
	\item[Operation] \texttt{N.A.}
	\item[OpCode] \texttt{0b010110xxxxxxxxxx} (see Table \ref{tab:wait}) 
	\item[Address Modes] None
	\item[Affected Flags] None
	\item[Cycles] 4 Clock Cycles
	\item No Operation, waits 4 clock cycles
\end{basedescript}

\subsection{Wait \texttt{WAIT}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{WAIT}
	\item[Operation] \texttt{N.A.}
	\item[OpCode] \texttt{0b010010xxxxxxxxxx} (see Table \ref{tab:wait}) 
	\item[Address Modes] None
	\item[Affected Flags] None
	\item[Cycles] N Clock Cycles
	\item No Operation, waits for interrupt signal
\end{basedescript}

\subsection{DMA Access \texttt{DMA}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{DMA}
	\item[Operation] \texttt{N.A.}
	\item[OpCode] \texttt{0b110011xxxxxxxxxx} (see Table \ref{tab:wait}) 
	\item[Address Modes] None
	\item[Affected Flags] None
	\item[Cycles] N Clock Cycles
	\item No Operation, hands over control of the Databus to the DMA Controller, waits until DMA controller pulls the signal DMA Busy \texttt{DMAB} to low.
\end{basedescript}

\newpage
\section{Arithmic Logic Operations}

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
	\end{bytefield}\\
	\begin{description}
		\item[Rd] Destination Register
	\end{description}
	\caption{Single Operand Arithmetic Logical Operation Layout}
	\label{tab:ALU_single_op}
	\normalfont\normalsize
\end{figure}

\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{2}{OM} & \bitbox{8}{Control Code} \\
		\begin{rightwordgroup}{Register Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Rs}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Direct Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{1} & \bitbox{4}{Rd} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{1}{0} \\
			\bitbox{16}{Memory Value}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Absolute Address Mode}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Addr[19:16]}\\
			\bitbox{16}{Addr[15:0]}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{Other Modes}
			\bitbox{6}{OpCode} & \bitbox{1}{1} & \bitbox{1}{1} & \bitbox{4}{Rd} & \bitbox{4}{Addr Mode}\\
			\bitbox{16}{Memory Value (optional)}
		\end{rightwordgroup} \\
		\begin{rightwordgroup}{SB/RB}
			\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Bit/Flag}
		\end{rightwordgroup}
	\end{bytefield}\\
	\begin{description}
		\item[OM] Operation Mode
		\item[Rd] Destination Register
		\item[Rs] Source Register
		\item[Addr] 20 bit Address
		\item[Addr Mode] Other Modes (see Table \ref{tab:other_mode_8})
	\end{description}
	\caption{Two Operand Arithmetic Logical Operation Layouts}
	\label{tab:ALU_double_op}
	\normalfont\normalsize
\end{figure}

\vspace{1em}
\begin{figure}[h!]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{6}{OpCode} & \bitbox{1}{0} & \bitbox{1}{0} & \bitbox{4}{Rd} & \bitbox{4}{Flag} \\
	\end{bytefield}\\
	\begin{description}
		\item[Rd] Destination Register
	\end{description}
	\caption{Bit Manipulation Operation Layout}
	\label{tab:ALU_bit_op}
	\normalfont\normalsize
\end{figure}

\newpage
\subsection{Add \texttt{ADD}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{ADD Rd, Rs}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd + Rs}
	\item[OpCode] \texttt{0b100000xxxxxxxxxx} (see Table \ref{tab:ALU_double_op}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] C, OF/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Add the value of Register Rs to the value of Register Rd
\end{basedescript}

\subsection{Add with Carry \texttt{ADDC}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{ADDC Rd, Rs}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rs + Rd + C}
	\item[OpCode] \texttt{0b100001xxxxxxxxxx} (see Table \ref{tab:ALU_double_op}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] C, OF/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Add the value of Register Rs and the Carry bit to the value of Register Rd
\end{basedescript}

\subsection{Subtract \texttt{SUB}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{SUB Rd, Rs}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd - Rs}
	\item[OpCode] \texttt{0b100010xxxxxxxxxx} (see Table \ref{tab:ALU_double_op}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] C, OF, Z, N/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Subtract the value of Register Rs from the value of Register Rd
\end{basedescript}

\subsection{Subtract with Carry \texttt{SUBC}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{SUBC Rd, Rs}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd - Rs - C}
	\item[OpCode] \texttt{0b100011xxxxxxxxxx} (see Table \ref{tab:ALU_double_op}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] C, OF, Z, N/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Subtract the value of Register Rs and the Carry bit from the value of Register Rd
\end{basedescript}

\subsection{Increment \texttt{INC}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{INC Rd}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd + 1}
	\item[OpCode] \texttt{0b101000xxxxxxxxxx} (see Table \ref{tab:ALU_single_op}) 
	\item[Address Modes] None
	\item[Affected Flags] C, OF/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Increment the value of Register Rd by 1
\end{basedescript}

\subsection{Decrement \texttt{DEC}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{DEC Rd}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd - 1}
	\item[OpCode] \texttt{0b101001xxxxxxxxxx} (see Table \ref{tab:ALU_single_op}) 
	\item[Address Modes] None
	\item[Affected Flags] C, OF, Z, N/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Decrement the value of Register Rd by 1
\end{basedescript}

\subsection{Logic AND \texttt{AND}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{AND Rd, Rs}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd AND Rs}
	\item[OpCode] \texttt{0b110000xxxxxxxxxx} (see Table \ref{tab:ALU_double_op}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] Z/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Bitwise Logic And of Registers Rs and Rd, result saved in Rd
\end{basedescript}

\subsection{Logic OR \texttt{OR}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{OR Rd, Rs}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd OR Rs}
	\item[OpCode] \texttt{0b110001xxxxxxxxxx} (see Table \ref{tab:ALU_double_op}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] Z/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Bitwise Logic OR of Registers Rs and Rd, result saved in Rd
\end{basedescript}

\subsection{Logic XOR \texttt{XOR}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{ADD Rd, Rs}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd XOR Rs}
	\item[OpCode] \texttt{0b110010xxxxxxxxxx} (see Table \ref{tab:ALU_double_op}) 
	\item[Address Modes] All (see Table \ref{tab:memory_address_mode_8})
	\item[Affected Flags] Z/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Bitwise XOR of Registers Rs and Rd, result saved in Rd
\end{basedescript}

\subsection{Logic NOT \texttt{NOT}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{NOT Rd}
	\item[Operation] \texttt{Rd $\Leftarrow$ NOT Rd}
	\item[OpCode] \texttt{0b111000xxxxxxxxxx} (see Table \ref{tab:ALU_single_op}) 
	\item[Address Modes] None
	\item[Affected Flags] Z/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Logic NOT of Register Rd
\end{basedescript}

\subsection{Logic Left Shift \texttt{LLS}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{LLS Rd}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd << 1; LSB $\Leftarrow$ Carry; Carry $\Leftarrow$ MSB}
	\item[OpCode] \texttt{0b111001xxxxxxxxxx} (see Table \ref{tab:ALU_single_op}) 
	\item[Address Modes] None
	\item[Affected Flags] Z, C/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Logic Left Shift of Register Rd by one bit, Most Significant Bit gets replaced by Carry, Carry gets saved into Lowest Significant Bit
\end{basedescript}

\subsection{Logic Right Shift \texttt{LRS}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{LRS Rd}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd >> 1; MSB $\Leftarrow$ Carry; Carry $\Leftarrow$ LSB}
	\item[OpCode] \texttt{0b111010xxxxxxxxxx} (see Table \ref{tab:ALU_single_op}) 
	\item[Address Modes] None
	\item[Affected Flags] Z, C/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Logic Right Shift of Register Rd by one bit, Least Significant Bit gets replaced by Carry, Carry gets saved into Most Significant Bit
\end{basedescript}

\subsection{Logic Left Rotate \texttt{LLR}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{LLS Rd}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd << 1; LSB $\Leftarrow$ MSB}
	\item[OpCode] \texttt{0b111011xxxxxxxxxx} (see Table \ref{tab:ALU_single_op}) 
	\item[Address Modes] None
	\item[Affected Flags] Z, C/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Logic Left Rotate of Register Rd by one bit, Most Significant Bit gets moved into Lowest Significant Bit
\end{basedescript}

\subsection{Logic Right Rotate \texttt{LRR}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{LRS Rd}
	\item[Operation] \texttt{Rd $\Leftarrow$ Rd >> 1; MSB $\Leftarrow$ LSB}
	\item[OpCode] \texttt{0b111100xxxxxxxxxx} (see Table \ref{tab:ALU_single_op}) 
	\item[Address Modes] None
	\item[Affected Flags] Z, C/All (if target is the Flag Register R15)
	\item[Cycles] 6 Clock Cycles
	\item Logic Right Shift of Register Rd by one bit, Least Significant Bit gets moved into Most Significant Bit
\end{basedescript}

\subsection{Set Bit \texttt{SB}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{SB Rd, F}
	\item[Operation] \texttt{Rd[F] $\Leftarrow$ 1}
	\item[OpCode] \texttt{0b110011xxxxxxxxxx} (see Table \ref{tab:ALU_bit_op}) 
	\item[Address Modes] None
	\item[Affected Flags] Any
	\item[Cycles] 6 Clock Cycles
	\item Set bit F of Register Rd
\end{basedescript}

\subsection{Reset Bit \texttt{RB}}

\begin{basedescript}{\desclabelstyle{\pushlabel}\desclabelwidth{7em}}
	\item[Mnemonic] \texttt{RB Rd, F}
	\item[Operation] \texttt{Rd[F] $\Leftarrow$ 0}
	\item[OpCode] \texttt{0b110100xxxxxxxxxx} (see Table \ref{tab:ALU_bit_op}) 
	\item[Address Modes] None
	\item[Affected Flags] Any
	\item[Cycles] 6 Clock Cycles
	\item Reset bit F of Register Rd
\end{basedescript}