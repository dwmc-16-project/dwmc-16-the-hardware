\chapter{CPU Design}

This section is about the design of the CPU of the \dwmc. It will discuss the design of the Control Logic, the ALU and the Registers.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.65\textwidth]{images/CPU_Blockdiagram}
	\caption{Basic Blockdiagramm of the \dwmc}
\end{figure}

\section{ALU}

The ALU is one of the core components of the CPU, responsible for computing both arithmetic and logic operations, such as \texttt{ADD} and \texttt{AND}. It is also used to compare two operands for branch operations like \texttt{BEQ}.

It has two buffered inputs $ A $ and $ B $, as well as a buffered output $ O $, as well as input for control signals to select the operations of the ALU, as well as controlling the input and output buffers.

Input $ A $ is directly connected to only the General Purpose registers, and uses a up-down shift register to latch the value, controlled by the $ AAE $ control signal to load the data, as well as the Carry flag $ CIn $ . Several control signals are used to control the Shift Register $ SHCLK $, $ SHDIR $ and $ SHCC $. Additionally the Carry Flag can be put into the ALU with the $ ACE $ signal.

Input $ B $ is connected to the Data Bus with a buffer D-latch controlled by the $ ABE $ control signal. $ B $ is only connected to the Bus, allowing it to take inputs from memory, as well as from the General Purpose Registers. This allows to reduce the difference between control operations between using inputs to operate on either memory or registers as the second input. 

The output $ O $ is connected to a 2:1 multiplexer to select between the ALU output and the output of the Shift Register, with the $ OSEL $ Signal. It includes the Carry Flag $ COut $, as it is possible for both the ALU and the Shift Register to export a Carry Flag. The output from the Multiplexer is directly tied into the General Purpose Registers through ab buffer D-latch controlled by  the $ AOE $ control signal. During branch operations, it is not active to transfer the output into the General Purpose Registers, but instead only changes the flags of the Flag Register.

Additionally fo the Carry Flag, the ALU and logic tied to it may create several flags, $ Z $, $ EQ $, $ N $ and $ OF $.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\textwidth]{images/ALUBlockdiagram}
	\caption{ALU Blockdiagramm}
\end{figure}

\begin{table}[h!]
	\centering
	\ttfamily\scriptsize
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Signal} & \textbf{Signal Source} &                                                                 \\ \hline\hline
		    $ AAE $     &     Control Logic      & \makecell{ALU Input A Enable\\(from General Purpose Registers)} \\ \hline
		    $ ABE $     &     Control Logic      &         \makecell{ALU Input B Enable\\(from Data Bus)}          \\ \hline
		    $ AOE $     &     Control Logic      &  \makecell{ALU Output Enable\\(to General Purpose Registers)}   \\ \hline
		   $ SHCLK $    &     Control Logic      &              Shift Register Clock (Enables Shift)               \\ \hline
		   $ SHDIR $    &     Control Logic      &                    Shift Register Direction                     \\ \hline
		   $ SHCC $     &     Control Logic      &                  Shift Register Carry Control                   \\ \hline
		   $ OSEL $     &     Control Logic      &                          Output Select                          \\ \hline
		 $ AO0 - AO4 $  &     Control Logic      &                \makecell{ALU Operation\\(5 bit)}                \\ \hline
		    $ CIn $     &          Flag          &                            Carry In                             \\ \hline
		   $ COut $     &          Flag          &                            Carry Out                            \\ \hline
		     $ Z $      &          Flag          &                            Zero Flag                            \\ \hline
		    $ EQ $      &          Flag          &                           Equal Flag                            \\ \hline
		     $ N $      &          Flag          &                          Negative Flag                          \\ \hline
		    $ OF $      &          Flag          &                          Overflow Flag                          \\ \hline
	\end{tabular}
	\normalfont\normalsize
	\caption{ALU Control Signals}
\end{table}

\subsection{Possible Implementation}

Originally it was the intention to design the ALU from scratch, however it was decided that it would be better to simply make use of existing 74 series TTL logic ICs for the ALU. If it is good enough for DEC to use on the PDP-11 and VAX workstation, it is good enough for the \dwmc.

As such the ALU will be utilizing a set of 4 74181 4-bit arithmetic logic unit and function generator in combination with a single 74182 lookahead carry generator. For 74373 octal latch ICs will be used as input and output latches.

For the Shift Register, four 74194 4-bit bidirectional universal shift register will be used, with four 74157 2:1 Multiplexers for the output selection.

Finally, some logic will have to be used to support the shift register to allow rotation and shifting through Carry, as well as generating the Flags.

\section{General Purpose Registers}

The General Purpose Registers are a set of 16 bit registers, separated into two banks of 16 registers, \texttt{R00} to \texttt{R15} and \texttt{R00'} to \texttt{R15'}, the latter taken up by the Special Purpose Registers. They are used to store data internally for quick access by the ALU and the Control Logic.

The ALU or the Data Bus can only access one of the two register banks at a time and to access the other bank it has to be deliberately switched by setting the \texttt{RB} flag in the Flag Register, which sets/resets the $ SELRB $ signal without involving the Control Logic.

Of these registers, \texttt{R15} and \texttt{R15'} are actually designated the Flag Register \texttt{F}, which can be switched around, but has several signals shared across both Register banks.

\vspace{1em}
\begin{table}[h]
	\centering
	\ttfamily\footnotesize
	\begin{tabular}{|c|c|c|c|c|c|c|c|}
		\hline 
		R00 & R01 & R02 & R03 & R04 & R05 & R06 & R07 \\ 
		\hline 
		R08 & R09 & R10 & R11 & R12 & R13 & R14 & R15/F \\ 
		\hline 
	\end{tabular}  
	\normalfont\normalsize
	\caption{Register Bank}
\end{table}

\begin{figure}[h]
	\centering
	\ttfamily\footnotesize
	\begin{bytefield}[endianness=big, bitwidth=1.75em]{16}
		\bitheader{0-15} \\
		\bitbox{1}{Z} & \bitbox{1}[bgcolor=lightgray]{} & \bitbox{1}{C} & \bitbox{1}[bgcolor=lightgray]{} &
		\bitbox{1}[bgcolor=lightgray]{} & \bitbox{1}{OF} & \bitbox{1}{N} & \bitbox{1}{EQ} &
		\bitbox{1}[bgcolor=lightgray]{} & \bitbox{1}[bgcolor=lightgray]{} & \bitbox{1}[bgcolor=lightgray]{} & \bitbox{1}{RB} &
		\bitbox{1}{IE} & \bitbox{1}{I2} & \bitbox{1}{I1} & \bitbox{1}{I0}
	\end{bytefield}
	\caption{Flag Register Layout}
	\normalfont\normalsize
\end{figure}

\begin{table}[h!]
	\centering
	\ttfamily\small
	\begin{tabular}{|c|c|c|c|}
		\hline 
		Z & Zero Flag & RB & Register Bank Select \\ 
		\hline 
		C & Carry Flag & IE & Interrupt Enable Flag \\ 
		\hline 
		EQ & Equal Flag & I2 & Interrupt Flag 2 \\ 
		\hline 
		N & Negative Flag & I1 & Interrupt Flag 1 \\ 
		\hline 
		OF & Overflow Flag & I0 & Interrupt Flag 0 \\ 
		\hline
	\end{tabular} 
	\normalfont\normalsize
	\caption{Flags}
\end{table}

Of the flags, $ RB $, $ IE $ and $ I2 - I0 $ are shared across the Register Banks.

The ALU can take inputs from either the Data Bus or the ALU, and output to either the ALU or the Data Bus. The signals $ RRE $, $ RRT $, $ RWE $ and $ RWT $ are used to control the input and output.

\begin{table}[h!]
	\centering
	\ttfamily\scriptsize
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Signal}  & \textbf{Signal Source} &                                     \\ \hline\hline
		$ RRE $      &     Control Logic      &        Register Read Enable         \\ \hline
		$ RRT $      &     Control Logic      &        Register Read Target         \\ \hline
		$ RWE $      &     Control Logic      &        Register Write Enable        \\ \hline
		$ RWT $      &     Control Logic      &        Register Write Target        \\ \hline
		$ SELRB $     &     Flag Register      &        Select Register Bank         \\ \hline
		$ SELR0 - SELR3 $ &     Control Logic      & \makecell{Register Select\\(4 bit)} \\ \hline
	\end{tabular}
	\normalfont\normalsize
	\caption{General Purpose Register Control Signals}
\end{table}

\subsection{Possible Implementation}

There are two possible implementations for the General Purpose Registers.

The first implementation is to use eight 74189 64-bit RAM (16x4) ICs in combination with glue logic. However, it is hard to access this time of IC at the time of writing.

The second possible implementation is the use of 74373 octal latch ICs with glue logic, as it is easier to get access to these ICs at time of writing. It will, however, make the PCB for the General Purpose Register Board larger.

For both implementations, three 74373 are to be used for the Flag Register, to enable switching between half of the register, while the second half remains shared across banks.

\section{Special Purpose Registers}

The Special Purpose Registers are a set of different registers that are used for special purposes by the CPU. They are located in the second Register Bank, covering \texttt{R00'} to \texttt{R15'}. They can be switched out for \texttt{R00} to \texttt{R15}, as to make work with them slightly easier.

\begin{table}[h]
	\centering
	\ttfamily\scriptsize
	\begin{tabular}{|c|c|c|c|c|c|c|c|}
		\hline
		R00'/PCL & R01'/PCH & R02'/SPL & R03'/SPH & R04'/WL & R05'/WH & R06/'XL & R07'XH  \\ \hline
		R08'/YL  & R09'/YH  & R10'/ZL  & R11'/ZL  & R12'/WO & R13'/XO & R14'/YO & R15'/ZO \\ \hline
	\end{tabular}\\[0.75em]
	\begin{tabular}{|c|c|}
		\hline
		         PCL/PCH           &    Programm Counter     \\ \hline
		         SPL/SPH           &  System Stack Pointer   \\ \hline
		WL/WH, XL/XH, YL/YH, ZL/ZH & W/X/Y/Z Index Register  \\ \hline
		      WO, XO, YO, ZO       & W/X/Y/Z Offset Register \\ \hline
	\end{tabular}
	\normalfont\normalsize
	\caption{Special Use Register Bank}
\end{table}

\begin{description}
	\item[Program Counter] The Program Counter is a 20 bit register that contains the current address that is accessed by the CPU. The Program counter can be increased automatically by 1, 2 or 3. It can also be read and written to. Its control signals are $ PCI $, $ PCWA $, $ CS $, $ WE $ and $ RE $.
	\item[System Stack Pointer] The System Stack Pointer is a 20 bit register that contains the current address of the Systems Stack. The Stack Pointer can be automatically increased and decreased by 1. It can also be read and written to. Its control signals are $ SPID1 - SPID3 $, $ SPWA1 - SPWA3 $, $ CS $, $ WE $ and $ RE $.
	\item[W/X/Y/Z Index Pointer] The Index Pointers are 20 bit registers that contain a memory address that can be used by the CPU for indirect addressing of the main memory. It can be read and written to. Their control signals are $ W/X/Y/ZWA $, $ CS $, $ WE $ and $ RE $.
	\item[W/X/Y/Z Offset Register] The Offset Registers are 16 bit registers that contain an offset that is automatically added to the corresponding Index Register when the appropriate memory addressing method is selected. Their control signals are $ CS $, $ WE $ and $ RE $. To add to their index registers, the control signal $ ORA $. The selection of the correct Offset Register is done by the $ W/X/Y/ZWA $ signals. Additionally, they can be automatically incremented and decremented by the $ ORI $ and $ ORD $ signals in combination with the $ W/X/Y/ZWA $ signals.
	\item[Secondary Stack Pointer W/X/Y/Z] The Index Registers and Offset Registers in combination can act as additional Secondary Stack Pointers.
\end{description}

\begin{table}[h!]
	\centering
	\ttfamily\scriptsize
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Signal} & \textbf{Signal Source} &                                   \\ \hline\hline
		    $ CS $      &     Control Logic      &            Chip Select            \\ \hline
		    $ WE $      &     Control Logic      &           Write Enable            \\ \hline
		    $ RE $      &     Control Logic      &            Read Enable            \\ \hline
		    $ PCI $     &     Control Logic      &     Program Counter Increment     \\ \hline
		    $ PCWA$     &     Control Logic      &   Program Counter Write Address   \\ \hline
		    $ PCA $     &     Control Logic      &    Program Counter Offset Add     \\ \hline
		   $ SPID $     &     Control Logic      & Stack Pointer Increment/Decrement \\ \hline
		   $ SPWA $     &     Control Logic      &    Stack Pointer Write Address    \\ \hline
		 $ W/X/Y/ZWA $  &     Control Logic      &   Z Index Pointer Write Address   \\ \hline
		    $ ORA $     &     Control Logic      &        Offset Register Add        \\ \hline
		    $ ORI $     &     Control Logic      &     Offset Register Increment     \\ \hline
		    $ ORD $     &     Control Logic      &     Offset Register Decrement     \\ \hline
	\end{tabular}
	\caption{Special Use Register Control Signals}
\end{table}

\subsection{Possible Implementation}

\begin{description}
	\item[Program Counter] Five 74191 synchronous presettable up/down 4-bit binary counter can be used to implement the Program Counter.
	\item[System Stack Pointer] Five 74191 synchronous presettable up/down 4-bit binary counter can be used to implement the Stack Pointer.
	\item[W/X/Y/Z Index Pointer] Five 74373 octal latches can be used to implement an Index Register.
	\item[W/X/Y/Z Offest Register] 74191 synchronous presettable up/down 4-bit binary counter can be used to implement the Offset Registers, in combination with five 74283 4-bit binary full adders can be used, with a single 2:1 multiplexer to select between the adder and the Index Registers. This allows the use as Secondary Stack Pointers.
\end{description}

\section{Control Logic}

The Control Logic of the \dwmc is a microcode based state machine Control Logic, using a reprogrammable microcode instruction memory to save the microcode instructions to save the micro instructions used for the state machine.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{ControlLogic}
	\caption{Control Logic Block Diagram}
\end{figure}

\subsection{Operation}

The design Control Logic is heavily based on its operation.

At the beginning of an operation cycle, the Control Logic reads the current instruction from the Data Bus, from the address provided by the Program Counter, saving it to the Instruction Register. From here, it gets split into parts that get decoded by both the Instruction Decoder and the Control Code Decoder.

The Control Code Decoder then pushes the data into Address Mode Decoder, from where the addressing modes of the instruction gets decoded, either saving one or two flags or registers into the Register/Flag Register for use as Register Select Signals, or into the Address Register, which in turn can read from the Data Bus and output to the Address Bus.

The Instruction Decode meanwhile resets the Microprogram counter, which uses a reset-able ring counter to address the Microinstruction Memory, and then provides additional addressing for the Microinstruction Memory, based on the instruction. The selected Microinstruction gets saved into the Microinstruction register.

From here, the Microinstruction is decoded, providing the control signals needed by the CPU and the rest of the computer. At the last microinstruction for a given macroinstructionm the control logic advanced the Program Counter and resets the Microprogram Counter in conjunction with the Instruction Decoder.

It should be possible to combine the Microinstruction Memory, Register and Decoder into a single reprogrammable memory. It should also be possible to integrate the Instruction Decoder into the same memory.

\subsection{Interrputs}

\begin{figure}[h!]
	\centering
	\ttfamily\scriptsize
	\begin{bytefield}{16}
		\memsection{000000}{}{2}{Reset Vector}\\
		\memsection{000002}{}{2}{Interrupt Vector 1}\\
		\memsection{000004}{}{2}{Interrupt Vector 2}\\
		\memsection{000006}{}{2}{Interrupt Vector 3}\\
		\memsection{000008}{}{2}{Interrupt Vector 4}\\
		\memsection{00000A}{}{2}{Interrupt Vector 5}\\
		\memsection{00000C}{}{2}{Interrupt Vector 6}\\
		\memsection{00000E}{}{2}{Interrupt Vector 7}\\
	\end{bytefield}
	\normalfont\normalsize
	\caption{Interrupt Vectors in Memory}
\end{figure}

Interrupts are a basic part of the Control Logic and are handled through an integrated Interrupt Logic, which is directly tied into the low byte of the Flag Register.

On receiving an external interrupt from the external control bus, it will decide the Interrupt signal into a 3 bit number, which is pushed into a 3 stage FIFO connected to the $ I0 - I2 $ Flags. After this, the Interrupt Logic waits for the current instruction to be completed, before setting the \texttt{IE} Flag and switching the systems Register Bank to \texttt{R00'} - \texttt{R15'}, before saving the Program Counter, Index Registers and Offset Registers to the Stack (possible a dedicated Interrupt Stack).

This is followed by selecting one of the Interrupt Vectors from memory, which is written to the Program Counter.

This is followed by the Interrupt Logic waiting until the Interrupt Service Routine is completed and the \texttt{RTI} Instruction returns from the Interrupt. Is the Interrupt FIFO is empty, i.e. no further interrupts need to be handled, the Interrupt Logic writes the Program Counter, Index and Offset registers back from the Stack, allowing the system to return to normal operation.

If another interrupt is in the Interrupt FIFO, the interrupt logic simply writes the new Interrupt Vector to the Program counter and jumps into the new Interrupt Service Routine.

A special Interrupt is the Reset Interrupt, which interrupts the current operation, sets the Program Counter to the address contained in the Reset Vector and resets the Microprogram Counter.

\subsection{Possible Implementation}

The possible implementation of the Control Logic should largely be based around either a set of EPROM, EEPROM or Flash memory ICs, purely based on the possible access time of the ICs in question. The number of ICs is dependent on the number of control signals needed for the system, while the size depends on the number of discrete steps needed for each macroinstriuction. All other components are dependent on other considerations that will come up after the design of the Instruction Set.

Currently one possibility for the memory is using one or more CY27H010, which are available with access times as low as 30ns, though they are likely overkill when it comes to their memory size. Another possibility is the use of the SST39SF010A Flash memory, which is available with 55ns or 70ns access time.

Another possibility would be to use static RAM ICs that are filled with the microcode from an external microcontroller on startup. This would also allow to access the microcontroller as an IO device to read and write the microcode, allowing the microcode to be modified at runtime from inside the \dwmc with a dedicated software package.