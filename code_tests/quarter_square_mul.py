'''
    This script is to test out and analyse the Quarter Square method for 
    hardware multiplication.

    The algorith goes as follows:

    mul = f(a+b) - f(a-b)
    f(x) = x²/4

    In Python the Quarter Square function f(x) = x²/4 can be expressed as 
    int(x*x)>>2.
'''

from rich.console import Console
from rich.table import Table


def quarter_square_generator(n: int) -> int:
    '''
        Generates a list of n Quarter Squares as a tuple of x and f(x)
    '''
    i = 0
    while i < n:
        yield (i, int(i*i) >> 2)
        i += 1


WIDTH = 13

table = Table("", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
              "12", "13", "14", "15",
              title=f'Quarter Squares List for {WIDTH} bit wide numbers')

table2 = Table("Address Before", "Value", "Address After", "Value",
               title='Addresses where a new nibble is used')

table3 = Table("Start Address", "End Adddress", "Start Result", "End Result",
               "Memory Size", title='Result Upper Byte changes and addresses')

i = 0
j = 0
t_list = []
x1, qs1 = 0, 0
x2, qs2 = 0, 0
k = 4
l = 1

for x, qs in quarter_square_generator(2**WIDTH):
    t_list.append(f"{qs:0>8X}")
    j += 1
    if j > 15:
        table.add_row(f"{i:0>4X}X", t_list[0], t_list[1], t_list[2], t_list[3],
                      t_list[4], t_list[5], t_list[6], t_list[7], t_list[8],
                      t_list[9], t_list[10], t_list[11], t_list[12], 
                      t_list[13], t_list[14], t_list[15])
        i += 1
        j = 0
        t_list = []
    if qs >> k > 0:
        table2.add_row(f"{x1:0>5X}", f"{qs1:0>8X}", f"{x:0>5X}", f"{qs:0>8X}")
        k += 4
    m = qs >> 24
    if m > 0:
        if m == l:
            table3.add_row(f"{x2:0>5X}", f"{x1:0>5X}", f"{qs2:0>8X}",
                           f"{qs1:0>8X}", f"{x1 - x2} bytes")
            x2, qs2 = x, qs
            l += 1
    x1, qs1 = x, qs
    

console = Console()
console.print(table, justify="center")
console.print(table2, justify="center")
console.print(table3, justify="center")
